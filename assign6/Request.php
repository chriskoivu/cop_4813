
<?php
    require_once('Database.php');
	
	Class Request extends Database {
		
	   public function __construct() {
		parent::__construct();
	   	if (session_status() !== PHP_SESSION_ACTIVE) {
     		  @session_start();
   		}
	   	
	        	      
	   }
	
	   /* 
	      input is an array of data values for a single record.
	       ie. values for task, description, completed, date_added
	       in their corresponding fields
	   */
	   
	   public function input_db_record($field_array) {
	        
	      /*convert form value for film length for database table*/
	      $film_lngth = $field_array['film_length_hours']."hr, " . 
	      $field_array['film_length_minutes'] . "min";   
         $field_array['film_length']=$film_lngth;	 
         /*remove prior entries that were seperated out */
         unset($field_array['film_length_hours']);
         unset($field_array['film_length_minutes']);
         $this->insert_record($field_array);
	     $this->close_db();
	     $this->goto_page('index.php'); 
	   }
	   
	   /* 
	     retrieves all records in the table	     
	   */
	   public function get_all_db_records() {	     	       
	      $data = $this->select_all();	         
	      $this->close_db();
	      return $data;
	   }
	   
	   /* 
	     updates a selected record, redirects to home
	     page after record is deleted. 
	   */
	   public function update_db_record($field_array, $id) {
	     // format film length value for database
		 $film_lngth = $field_array['film_length_hours']."hr, " . 	     $field_array['film_length_minutes'] . "min";   
		 // save formatted film length to field array
         $field_array['film_length']=$film_lngth;	
		 // remove the form elements from the field array
		 unset($field_array['film_length_hours']);
         unset($field_array['film_length_minutes']);
		 // update the database table
		 $this->update_record($field_array, $id);
	     $this->close_db();
		 $this->goto_page('index.php');
	   }
	   
	   /* 
	     deletes a selected record, redirects to home
	     page after record is deleted. 
	   */
	   public function delete_db_record($record_id) {
	        
	      $this->delete_record($record_id);        
	      $this->close_db();
		  $this->goto_page('index.php');
	   }
	
	   /* 
	      handles page redirection on a successful request
	      for this app, setting all requests as successful
	   */
	   private function goto_page($page_name) {	      
	      if (1) {                
	         header("Location: " . $page_name);
	      } 
	   }
	
	   /* 
	     Retrieves data saved to the session. Not using 
		 any GETS processing methods for security. ie. 
		 "http://path/to/script.php?a" as this exposes 
		 folder structure to potential hackers
	   */
	   public function get_session_data($key) {
	       if (isset($_SESSION['data'][$key])) {
	         return $_SESSION['data'][$key];
	       }
	       return NULL;
	   }
	
	   /* 
	     Saves data to the session. Not using any 
		 GETS processing methods for security. ie. 
		 "http://path/to/script.php?a" as this exposes 
		 folder structure to potential hackers
	   */
	   public function set_session_data($key, $data) {
	      $_SESSION['data'][$key] = $data;
	   }
	  
	   public function get_hrs_min($hr_min){		
		  $val = explode(",", $hr_min);
		  $hr = rtrim($val[0], "hr");
		  $min = rtrim($val[1], "min");
		  $result = Array (
		    'hrs'=>trim($hr),
			'minutes'=>trim($min)		  
		  );		  
		  return $result;		  
	   }	
	   
	   /* if user clicked cancel on page calling this function */
	   public function cancel_request(){
		  $this->goto_page('index.php'); 
	   }
	
	}

?>
