
<HTML>
<HEAD>
<TITLE>Christopher M Koivu - Assignment 6</TITLE>
<link rel="stylesheet" href="../css/style.css">
<link rel="stylesheet" href="../css/assign6.css">

<?php
   ini_set('display_errors', 1);
   ini_set('display_startup_errors', 1);
   error_reporting(E_ALL);
   require_once('Request.php');   
   $req = New Request();
   $records = $req->get_all_db_records();		   
   $req->set_session_data('records', $records);  
   
?>
 
 
</HEAD>
<BODY>
     <a href="../index.html"> < Home </a>
     <h1> Christopher M Koivu </h1>
	 <h1> Movie Database Application </h1>
	 
	 <div class="wrapper">  
	     <p>
		    This app is a Movie Database application. To add a Movie Title,
			click the "Add Title" button. To modify the movie information,
			click the "Modify" button. To remove a Movie Title, click the
			"Delete" button. This application was designed based on a Object
			Oriented Design using inheritance. There are no GET methods used 
			for form submission. Only POST form submissions are used. This 
			was done by saving the parameters to the PHP parameters to the 
			session for security reasons. 
		 </p>
	  </div>
	 
     <div class="wrapper" style="width:80%;">  
       <div style="margin:30px 0px;">
         <form action="addtitle.php" method="post">
         <input type="submit" value="Add Title"></form>
       </div>
	 </div>
	 <div class="table wrapper" style="width:80%;">
       <?php 
	     if(!empty($records)){
			 foreach ($records as $key=>$value){
			   /* if costars are entered, add a comma after star name */
			   if(!empty($value['costar'])){
			     $value['costar']=", " . $value['costar'];				 
			   }
			   echo ' <table style="width: 750px;">
						<tr>
						  <th>Title</th>
						  <td>' . trim($value['title']).
						  '</td>
						</tr>
						<tr>
						  <th>Description</th>
						  <td>
						  <p>' . trim($value['description']). 
						  '</p>
						  </td>
						</tr>
						<tr>
						 <th>Release Date</th>
						 <td>' . trim($value['release_date']).
						 '</td>
						</tr>
						<tr>
						 <th>Genre</th>
						  <td>'.
							trim($value['genre']).
						  '</td>
						</tr>
				   
						<tr>
						  <th>Film Length</th>
						  <td>'
							. trim($value['film_length']).
						  '</td>
						</tr>
						<tr>
						 <th>Director</th>
						 <td>'
						  . trim($value['director']).
						 '</td>
						</tr>
						<tr>
						  <th>Writers</th>
						  <td>'.
							trim($value['writer']).
						  '</td>
						</tr>
						<tr>
						  <th>Stars</th>
						  <td>' .
						  trim($value['star']). trim($value['costar']).
						  '</td>
						</tr>				
						<tr>
						  <th>Format</th>
						  <td>'. trim($value['format']).'</td>
						</tr>
						
					</table>';
					print '<table class="edit"><tr><td><form action="modifytitle.php" method="post">
						   <input type="hidden" name="modify" value="'
						   . $key . '"/>
						   <input type="submit" name = "modifytitle" 
						   value="Modify">
						   </form></td><td>
						 		   
						   <form action="deletetitle.php" method="post">
						   <input type="hidden" name="delete" value="'
						   . $key .'"/>
						   <input type="submit" name="deletetitle" value="Delete">
						   </form></td></tr></table>';
					$value['costar']= "";
			}
		 }else{
			print '<p id="error"> The database is empty. Please add a movie title.</p>';
		 }
	  ?>
    </div>




	
</BODY

</HTML>
