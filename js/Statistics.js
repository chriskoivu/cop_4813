	function submit() {
		var thisArray = read_input();
        document.getElementById("mean").innerHTML = "The mean is " + mean(thisArray).toFixed(2);	
		document.getElementById("mode").innerHTML = "The mode is " + mode(thisArray);	
		document.getElementById("stdev").innerHTML = "The standard deviation is " + stand_deviation(thisArray).toFixed(4);
		document.getElementById("variance").innerHTML = "The variance is " + variance(thisArray).toFixed(4);	
		document.getElementById("median").innerHTML = "The median is " + median(thisArray);
		document.getElementById("summation").innerHTML = "The summation is " + sum(thisArray);
		document.getElementById("count").innerHTML = "The count is " + count(thisArray);			 
	 }

  
	  function read_input() {		  
		  var arr = [];
		  var format =/^[0-9]+([,.][0-9]+)?$/g;
		  var x = trim_entry(document.getElementById("textarea").value);      
		  var numbers=x.split('\n');
		  for(var i=0;i<numbers.length;i++){  
			  if(numbers[i].match(format)) {
			   arr.push(numbers[i]);
			  }
		  }
		  return arr;
	   }
 
 
	   function trim_entry(x) {
		  return x.replace(/^\s+|\s+$/gm,'');
	   }

 
	function median(arr) {		
		var median = 0;
		array_length = arr.length;
		arr.sort(function(a, b){return a - b}); /* sort ascending */
	 
	    /* if array length is even */
		if (array_length % 2 === 0) {
			// average of the two middle numbers
			median = (Number(arr[array_length / 2 - 1]) + Number(arr[array_length / 2])) / 2;
		} else { // is odd
			// middle number only
			median = Number(arr[(array_length - 1) / 2]);
		}	 
		return median;
	}
 
   
	function variance(arr){    
		 var total = 0;
		 var calc = 0;
		 for (i = 0; i < arr.length; i++) { 
			total += Number(arr[i]);
		 }
		 var avg = total/arr.length;
		 for (i = 0; i < arr.length; i++) { 
			calc += Math.pow((parseFloat(arr[i]) - avg),2);
		 }
		 return calc / arr.length;     
	}


   function count(arr) {      
     return arr.length;
   }


   function sum(arr){  
     var total = 0;
     for (i = 0; i < arr.length; i++) { 
        total += Number(arr[i]);
     }
     return total;
   }

   function mean(arr){
     var count = arr.length;
     var total = 0;
     for (i = 0; i < count; i++) { 
        total += Number(arr[i]);
     }
     return total/count;
   }
 
   function stand_deviation(arr) {
     sdCalc = 0;     
     var count = arr.length;
     mean = mean(arr);
     for (i = 0; i < count; i++) {   
        sdCalc += Math.pow((parseFloat(arr[i]) - mean),2);        
     }
     return Math.sqrt(sdCalc/count);
   }


   function mode(arr) { 

    /* creating hash table to store each matching number */
    var hash = {};    
 
    /* hash table key & for loop counter */ 
    var key;

    /* counter for each number occurrence */    
    var frequency = 1;
 
    /* set mode to null in case there is no mode */
    var mode = null;

    /* to compare last number to next number */
    var last = -Infinity;
    
    /* sort the passed array by value, ascending order */
    /* using a closure because arrays are sorted as strings */
    arr.sort(function(a, b){return a - b});
   
     
    for (key = 0; key < arr.length; key++) {    
        /* if the last number is the same as this number, save value to hash */ 
        if (arr[key]==last) { 
          /* using the matched number as a hash key */
          hash[last] = ++frequency;                           
        } else {
          /* mode changed, reset frequency counter */
          frequency = 1;
          /* save last number tested */
          last = arr[key];  
        }         
    }   

    /* clear frequency for hash table transversal */
    frequency = 0;
    /* search hash table for most frequent number */
    for (key in hash) {         
      /* the key with the highest frequency is the mode */   
      if (hash[key] > frequency) {
        frequency = hash[key];
        mode = key;
	  /* if multiple modes, aggregate them */
      } else if (hash[key] == frequency) {
        mode += ", " + key;
      }
    }
    return mode;
} 
