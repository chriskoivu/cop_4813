	$(document).ready(function () {
    	var canvas = document.getElementById("canvas");
		var ctx = canvas.getContext("2d");
		var i = 0;
		var image = new Image();
		image.src = "../images/bar_magnet.jpg";
		var fps = 48;
	 
		animate();

		function animate() {	 
		  setTimeout(function() {
			i++;	 
			// limit animation to two full rotations
			if (i/360 == 2) {		  
			  return;
			}
			ctx.clearRect(200,90,400,400);
			left_magnet();
			img_rotate(image,350,100,53,224,i);
			right_magnet();
			 add_caption(350, 425, "FIELD MAGNET");
			animate();
		  }, 1000/fps);
		}
		function add_caption(x, y, caption) {				  	  
		  ctx.fillText(caption, x, y);		  
		}

		function img_rotate(img,x,y,width,height,deg){
			if (deg == 10) 				 
				add_caption(250, 50, "Opposite poles attract");     
                			
			if (deg == 230)                     	
				 add_caption(250, 70, "Like poles repel");
			//Convert degrees to radian 
			var rad = deg * Math.PI / 180;
		   //Set the origin to the center of the image
			ctx.translate(x + width / 2, y + height / 2);
			//Rotate the canvas around the origin
			ctx.rotate(rad);
			//draw the image         
			ctx.drawImage(img,-(width / 2), -(height / 2),width,height);
			//reset the canvas      
			ctx.rotate(-rad);    
			ctx.translate(-(x + width / 2), -(y + height / 2));
		}
	 


		function left_magnet( ) {
			 ctx.beginPath();
			 ctx.moveTo(200, 100);       
			 ctx.lineTo(200, 400);
			 ctx.lineTo(375, 400);
			 ctx.lineTo(375, 360);
			 ctx.lineTo(250, 360);
			 ctx.lineTo(250, 140);
			 ctx.lineTo(250, 140);
			 ctx.lineTo(250, 100);
			 ctx.lineTo(200, 100);            
			 ctx.stroke();
			 ctx.fillStyle = "red";
			 ctx.fill();
			 ctx.closePath(); 
			 ctx.beginPath();
			 ctx.rect(250,100,20,40);
			 ctx.stroke();  
		}

		function right_magnet() {
			ctx.beginPath();
			ctx.moveTo(550, 100);
			ctx.lineTo(500, 100);
			ctx.lineTo(500,140);
			ctx.lineTo(500,140);
			ctx.lineTo(500,360);
			ctx.lineTo(375,360);
			ctx.lineTo(375,400); 
			ctx.lineTo(550,400); 
			ctx.lineTo(550,100);              
			ctx.stroke();
			ctx.fillStyle = "blue";
			ctx.fill();
			ctx.closePath(); 
			ctx.beginPath();
			ctx.rect(480,100,20,40);
			ctx.stroke(); 
		}
	});