<?php


  class Stock 
  {

      
       /* 
         Stock constructor.  setting portfolio
         session variable for persistent data		 
       */

        public function __construct (){
           $file = 'stocks.txt';
		   if (!isset($_SESSION['portfolio']))
		      $_SESSION['portfolio']= Array();
		   if ( file_exists($file)) 			 		   
			 $this->read_stock_data($file);	 
        }
      
        private function get_time() {
           $date = new DateTime('America/New_York');
           return $date->format('m-d-y H:i:s');
        }

        public function delete_stock($stock_name) {
			$stock_name = strtoupper($stock_name);
            if(isset($_SESSION['portfolio'][$stock_name])){
			  $_SESSION['portfolio'][$stock_name]['quantity']=0;
		    } else { 
			  /* this condition shouldnt happen in most cases */
			  echo "Stock: " . $stock_name . " does not exist";
			}
			$this->save_stocks();
        } 
        
			
        public function add_stock($stock_name, $qty) {
			  $stock_name = strtoupper($stock_name);
			  $arr = Array();
			  $arr['stockname'] =  strtoupper($stock_name);
			  $arr['quantity'] = $qty;
			  $arr['shareprice'] = $this->get_price($stock_name);
			  $arr['adddate'] = $this->get_time();
			  
			  /* same stock symbol entered. add to existing qty */
			  if(isset($_SESSION['portfolio'][$stock_name])){
				$_SESSION['portfolio'][$stock_name]['quantity']+=$qty;
			  } else {
				$_SESSION['portfolio'][$stock_name]=$arr;
			  }			 
			 $this->save_stocks();
			 $req = New Request(isset($_SESSION['user']));
			 $req->process_request('admin.php');
        } 

        public function modify_stock($stock_name, $qty) {     
            $stock_name = strtoupper($stock_name);		
		    if(isset($_SESSION['portfolio'][$stock_name])){
			 $_SESSION['portfolio'][$stock_name]['quantity']=$qty;
		    } else {
			  echo "Stock: " . $stock_name . " does not exist";
			}
			
			$this->save_stocks();
        }  
		
		/* write the portfolio to file */		
		private function save_stocks(){			
			$this->write_stock_data($_SESSION['portfolio']);
		}
  
        /* retrieves stock array set */		
		public function retrieve_stocks() {
			return $_SESSION['portfolio'];
		}
        /* 
            empties contents of our stock portfolio
         */

		public function clear_portfolio() {
			/* clear the session array */
		     $_SESSION['portfolio']=NULL;
		}
        
             
         /* get the current stock price from yahoo finance */
         private function get_price($stock) {       
           $stock  = strtoupper($stock); 
		   $url="http://finance.yahoo.com/d/quotes.csv?s=" .$stock."&f=sl1d1t1c1ohgv&e=.csv";	 
           $file = fopen($url,"r");		
		   /* set file buffer size to 2k */
           $arr = explode(",",fread($file,2048));
           fclose($file);
		   $dollars = $arr[1];	
           return $dollars;	
        }

         /* 
           reads stock data from the data file
           and sets the values in session
        */
        private function read_stock_data($file) {	
             $f = new Datafile($file);
             $lines = $f->get_file_data(); 
             $stocks = Array();
             foreach ($lines as $value) {   
			   parse_str($value, $output);    
			   /* 
			      set key to stock symbol, and save parsed value array 
			      if stock name is not null
			   */  
               if(isset($output['stockname'])){				   
				   $_SESSION['portfolio'][$output['stockname']] = $output;  
               }			   
             }
		}    
       
        /* write stocks array to file */
        private function write_stock_data($array) {
           $output = array();
		   /* loops through all the arrays in our session array */
		   foreach ($_SESSION['portfolio'] as $key=>$value) {  
              /* only write lines with an actual stock */		   
              if(isset($value['stockname'])){
				  $line = "stockname=" . $value['stockname']."&quantity=" . $value['quantity'].
				  "&shareprice=" . $value['shareprice']. "&adddate=" . $value['adddate'];
				  $output[]=$line;
			  }
           }
		  $f = new Datafile('stocks.txt');
		  $f->write_file($output);
        }
  }
?>