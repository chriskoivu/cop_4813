<!DOCTYPE HTML>
<html>
  <head>
     <title>Chris Koivu - Assignment 5</title>	 
     <link rel="stylesheet" href="../css/style.css">
     <?php
	    session_start();
	    require_once('User.php');
	    require_once('Datafile.php');
	    require_once('Request.php');
     ?>
	 
	 <style>
	   table{
		   margin-left: auto;
           margin-right: auto;
       }
	   #error {
		 color: red;
		 background-color:white;
		 width:25%;
		 margin-left: auto;
         margin-right: auto;
	   }  
	 </style>
  </head>
 
 
  <body>
     <a href="../index.html"> < Home </a>
     <div class="wrapper">            
     	<form method="post">
		    <h1> Please Login: </h1><br>
			<table>
				<tr><td>
				User ID:</td><td> <input type="text" name="userid"> 
				</td></tr>
				<tr><td>
				Password:</td><td> <input type="password" name="password"> 
				</td></tr>
				<tr><td>
				<input type="submit" name = "submit" value="Login">
				</td></tr>
			</table>
		</form>
		 <?php
		   
		   if ( isset( $_POST['submit']) ) { 		     
		     $usr = New User($_POST['userid'], $_POST['password']);
			 if($usr->is_logged_in()){
                /* flag as valid request, as user is logged in */				 
			    $req = New Request($usr->is_logged_in());
			    $req->process_request('admin.php');
			 }else{
                  print "<div class='wrapper'><p id='error'>Invalid User ID and Password Combination</p></div>".PHP_EOL;
             }		
		   }
		 ?>
	  </div>
  </body>  
</html>




