<?php
       Class User {
       /* hashed password used for this app, includes salt */
       // private $pw_hash = '$2y$10$n2X/GQh1rJgd88Uc2NTT3Oc.jjCZp0kWnOUlM1KA0EiKC3cYJy3gG';

       /* user entered password */
        private $pw;

       /* user entered user id */
        private $uid;  

        /* users array for storing external user file data */
        private $stored_users = array();

        /* external users file. not using db for this project */
        private $file = "users.txt";
             
		


        public function __construct($user_id, $user_pass){   
          $this->uid = $user_id;
          $this->pw = $user_pass;
		  unset($_POST);	          		  
		  $this->get_user_data();		  	
        }

        private function hash_password($password){
           return password_hash("rasmuslerdorf", PASSWORD_DEFAULT);
        }
        
        private function is_password_valid($password) {    
           if(trim($this->uid) != false){		
			 if(password_verify ($password, $this->get_stored_password($this->uid))) {
				return true;
			 }
		   }
           return false;
	   }

        public function is_logged_in() {
           if($this->is_password_valid($this->pw)){  
              $_SESSION['user']=$this->uid;  
			  return true;
           }
		   $_SESSION['user']=NULL;
           return false;
        }
        
		public function get_user_id(){
			if(isset($_SESSION['user'])) {
				return $_SESSION['user'];
			} else {
			  return false;
			}
		}
		public function log_out() {
			$_SESSION['user']=NULL;
			
		}
		
		
        public function display_user_name() {
           echo 'Welcome ' . $this->uid;
        }
                 
         
        private function get_stored_password($user_id) {  
           	
           return $this->stored_users[$user_id];            
        }

        
        /* 
           retrieve current user data from file 
           store in array            
        */
        private function get_user_data() {            
            $ufile = new Datafile($this->file);
			$arr = $ufile->get_file_data(); 
  
            foreach ($arr as $value) { 
               parse_str($value, $output);      
               $this->stored_users[$output['user']] = $output['password'];
            }   
        }



   }
?>