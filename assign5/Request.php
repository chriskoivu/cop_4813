<?php


  class Request 
  {

       private $_valid_user;
       
       /* valid user is a boolean value  */
       public function __construct ($valid_user){
          $this->_valid_user = $valid_user;
		  if ($this->_valid_user == false) {       
			 $this->return_to_login();
		  }
       }
      

       /* redirect to login page */
       private function return_to_login(){
           header("Location: index.php"); 
		    session_destroy();
       }
      

       /* 
          if valid user, process request 
       */
       public function process_request($path) {
          if ($this->_valid_user == true) {      
			 echo "succesful request";
			 header("Location: " . $path);
		  }
       }

     

       
   }
?>