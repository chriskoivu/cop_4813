<!DOCTYPE HTML>
<html>
  <head>
     <title>Chris Koivu - Assignment 5</title>	 
     <link rel="stylesheet" href="../css/style.css">
     <?php
	    session_start();
	    require_once('Stock.php');
	    require_once('Datafile.php');
	    require_once('Request.php');
     ?>
  </head>
 
  <body>
      <?php
    	  if (isset($_SESSION['user'])){        
            echo "Welcome " . $_SESSION['user'] ."!"; 
			$stk = New Stock();
			if (isset($_POST['delete'])){			  
			  $_SESSION['symbol']= $_POST['delete'];
			}			
          }            
          $req = New Request(isset($_SESSION['user']));
	   ?>
	  <h1> Stock name: <?php echo $_SESSION['symbol'];?></h1>
      <div class="wrapper">            
     	<form method="post">
			<table>
				<tr><td>
				Are you sure you want to delete this stock?
				</td></tr>
				 
				<tr><td>
				  <input type="submit" name = "submit" value="Delete Stock">
				</td></tr>
			</table>
		</form>
		<?php
		   
		   if ( isset( $_POST['submit']) ) { 		      
			  $stk->delete_stock($_SESSION['symbol']);
		      $req->process_request('admin.php');
           }	
        ?>		  
	  </div>
  </body>  
</html>



