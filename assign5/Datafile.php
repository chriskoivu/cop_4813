<?php


   class Datafile
   {
       private $file;
       private $lines = array();
       
       public function __construct ($filename){
          $this->file = $filename;
		  
       }

      
       /* stores each line from text file into array */
       private function read_file() {
		   $myfile = fopen($this->file, "r");	  	    
	       while(!feof($myfile)) {
             $this->lines[]= fgets($myfile);  	      
	       }
           fclose($myfile); 
       }
      
       /* returns lines array   */
       public function get_file_data() {
		   $this->read_file();
           return $this->lines;
       }

       /* writes line array data to file */
       public function write_file($arr) {
           $myfile = fopen($this->file, "w");
	   
            foreach ($arr as $value) {                
               fwrite($myfile, $value . "\r\n");
            }
            fclose($myfile);
       }
 
   }
?>


 
