<!DOCTYPE HTML>
<html>
  <head>
     <title>Chris Koivu - Assignment 5</title>	 
     <link rel="stylesheet" href="../css/style.css">
     <?php
	    
		
		session_start();
	    require_once('Stock.php');
	    require_once('Datafile.php');
	    require_once('Request.php');
     ?>
  </head>
 
  <body>
      <?php
          if (isset($_SESSION['user'])){        
            echo "Welcome " . $_SESSION['user'] ."!"; 
			$stk = New Stock();
			$arr = Array(); 
		  }
		  
           $req = New Request(isset($_SESSION['user']));            
         
	   ?>
	   
	  <div class="wrapper">  
	     <p>
		     Add the stock symbol and the number of shares for
			 the stock you want to add.
		 </p>
	  </div>
	  
      <div class="wrapper">            
     	<form method="post">
			<table>
				<tr><td>
				Stock Symbol:</td><td> <input type="text" name="symbol"> 
				</td></tr>
				<tr><td>
				Number of Shares:</td><td> <input type="text" name="qty"> 
				</td></tr>
				<tr><td>
				<input type="submit" name = "submit" value="Add Stock">
				</td></tr>
			</table>
		</form>
		 <?php
		   
		   if ( isset( $_POST['submit']) ) { 
			 $stk->add_stock($_POST['symbol'], $_POST['qty']);
			 $req->process_request('admin.php');
		   }
		 ?>
	  </div>
  </body>  
</html>