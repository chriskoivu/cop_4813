<!DOCTYPE HTML>
<html>
  <head>
     <title>Chris Koivu - Assignment 5</title>	 
     <link rel="stylesheet" href="../css/style.css">
     
     <?php
	    session_start();
	    require_once('Stock.php');
	    require_once('Datafile.php');
	    require_once('Request.php');
     ?> 

  <style>
    td, th {
       padding:5px 20px;
       white-space:nowrap;
       border: none;
    }
 
    table {
       border-collapse: collapse;
    }

    .add {
       margin-right:auto;
       width:40%;
       margin-bottom:25px;
    }

  </style>
  </head>
 
  <body>  
     <div class="wrapper">            
		   <a href="../index.html"> < Home </a>
			 
		   <h1> Stock Portfolio Application </h1>
			   
		  <?php
			  if (isset($_SESSION['user'])){        
				echo "Welcome " . $_SESSION['user'] ."!<br>"; 
				$stk = New Stock();	           
			  } 
			  $req = New Request(isset($_SESSION['user'])); 
		   ?>  
	  </div>
	  <div class="wrapper">  
	     <p>
		    This app is a Stock Portfolio application. To add a stock,
			click the "Add Stock" button. To change the number of stocks,
			click the "Modify Stock" button. To remove a stock, click the
			"Delete Stock" button.
		 </p>
	  </div>
	  
      <div class="wrapper">           	  
         <div class="add">        
     	 <form action="addstock.php" method="post">
<input type="submit" name = "addstock" value="Add Stock"></form>
</div>


			<table >
				<tr style="background-color:gray;margin:0;">
				<th>Stock</th><th>Price</th><th>Number Of Shares</th><th>Total Price</th><th>Date Added</th>
                <th></th><th></th>
				</tr>				
				 
                <?php   
				   
                   $arr = $stk->retrieve_stocks();	
                  	
                  foreach ($arr as $key=>$value){    
                     if($value['quantity']>0)	{	?>		  
                     <tr>
					   <td> <?php echo $value['stockname']; ?> </td>
					   <td> <?php echo '$' . number_format((double)$value['shareprice'],2); ?> </td>
					   <td> <?php echo $value['quantity']; ?> </td>
					   <td> <?php echo '$' . number_format(($value['quantity']*$value['shareprice']),2); ?> </td>
					   <td> <?php echo $value['adddate']; ?> </td>
					   <td><form action="modifystock.php" method="post">
<input type="hidden" name="modify" value="<?php echo $value['stockname']; ?>"/>
<input type="submit" name = "modifystock" value="Modify Stock"></form></td>
					   <td> <form action="deletestock.php" method="post">
<input type="hidden" name="delete" value="<?php echo $value['stockname']; ?>"/>
<input type="submit" name = "deletestock" value="Delete Stock"></form></td>
					 </tr> 
                 <?php }  
                }	?>		 
			</table>
		
		 
	  </div>
  </body>  
</html>


